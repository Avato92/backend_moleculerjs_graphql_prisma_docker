>## Backend project with MoleculerJS + GraphQl + Prisma by Alejandro Vañó


## Getting started

### To get the Node server running locally:

- Clone this repo
- `npm install` to install all required dependencies
- `npm run dev` to start the local server
- the API is available at http://localhost:3000/api

if it gives you problems with the bcrypt package, instead of using npm to install, use yarn.

Another alternative is to change the package.json, the bcrypt for bcryptjs and then in the file users.service.js modify the constant: 
		const bcrypt = require ("bcrypt"); 
	by:
	 	const bcrypt = require ("bcryptjs");

#### To install GraphQL + Prisma on MoleculerJS

	- Install primsa CLI
		- npm install -g prisma
	- Install docker
	- Create a new directory and create configuration docker file
		- touch docker-compose.yml
	- Add Prisma and database Docker images
		- You can choose between MySQL, PostgresSQL, MongoDB...
	- Launch Prisma and the connected database
		- docker-compose up -d
		- At this point Prisma is connected to a local database and runs on (http://localhost:4466)
	- Configure your Prisma API
		- prisma init--endpoint http://localhost:4466
	- Deploy the Prisma datamodel
		- prisma deploy
	- Generate your Prisma Client
		- prisma generate

	You can read all the information about [Prisma.](https://www.prisma.io/docs/1.24/get-started/01-setting-up-prisma-new-database-JAVASCRIPT-a002/)


## Code Overview

### Dependencies

- [moleculer](https://github.com/ice-services/moleculer) - Microservices framework for NodeJS
- [moleculer-web](https://github.com/ice-services/moleculer-web) - Official API Gateway service for Moleculer
- [moleculer-db](https://github.com/ice-services/moleculer-db/tree/master/packages/moleculer-db#readme) - Database store service for Moleculer
- [moleculer-db-adapter-mongo](https://github.com/ice-services/moleculer-db/tree/master/packages/moleculer-db-adapter-mongo#readme) - Database store service for MongoDB *(optional)*
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) - For generating JWTs used by authentication
- [bcrypt](https://github.com/kelektiv/node.bcrypt.js) - Hashing user password
- [lodash](https://github.com/lodash/lodash) - Utility library
- [slug](https://github.com/dodo/node-slug) - For encoding titles into a URL-friendly format
- [nats](https://github.com/dodo/node-slug) - [NATS](https://nats.io) transport driver for Moleculer *(optional)*

### Application Structure

- `moleculer.config.js` - Moleculer ServiceBroker configuration file.
- `services/` - This folder contains the services.
- `public/` - This folder contains the front-end static files.