"use strict";

const { MoleculerClientError } = require("moleculer").Errors;
const { ForbiddenError } = require("moleculer-web").Errors;
const { prisma } = require('../prisma/generated/prisma-client');

module.exports = {
	name: "teams",

	settings: {
		},

	actions: {
		/**
		* This action is called when user open the teams view
		* and list all the sports and insert into a dropdown
		*/
		listSports: {
			async handler(ctx){
					const query = `query sport{
  											sports{
    											id
    											name
   										}
									}`
					const res = await prisma.$graphql(query);
					return res;
			}
		},
		listCategories: {
			params: {
				data: {
					type: "object", props:{
						id_sport: {type: "string"}
					}
				}

			},
			async handler(ctx){
					const query = `query whereSport($data: SportWhereInput){
  												sports(where:$data){
    														categories{
    															name
    															id
    														}
  												}
									}`
					const variable = {"data":{"id": ctx.params.data.id_sport}};
					const res = await prisma.$graphql(query, variable);
					return res;
			}
		},
		listTeams: {
			params: {
				data: {
					type: "object", props: {
						id_category : {type : "string"}
					}
				}
			},
			async handler(ctx){
				const query = `query whereCategory($where: CategoryWhereUniqueInput!){
  												category(where:$where){
    														name
														    id
														    team{
														      id
														      name
														      wins
														      draws
														      defeats
														      points
														    }
														  }
														}`
					const variable = {"where": {"id": ctx.params.data.id_category}};
					const res = await prisma.$graphql(query, variable);
					return res;
			}
		}
	}
}