"use strict";
const nodemailer = require("nodemailer");

module.exports = {
  name : "contact",

  settings : { 
  },
  actions: {
    sendEmail :{
      params : {
        data : {type: "object", props: {
                  email : {type: "string", min : 1},
                  subject : {type : "string", min : 1},
                  comment : {type : "string", min : 1}
              }
            }
          },
      handler(ctx){

        async function main(){
          // create reusable transporter object using the default SMTP transport
          let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            requireTLS: true,
            auth: {
              user: 'avatodaw@gmail.com', // generated ethereal user
              pass: 'pruebasdaw' // generated ethereal password
            }
          });
        
          // setup email data with unicode symbols
          //👻✔
          let mailOptions = {
            from: ctx.params.data.email, // sender address
            to: "avato92@gmail.com", // list of receivers
            subject: ctx.params.data.subject, // Subject line
            text: ctx.params.comment, // plain text body
            html: "<p>" + ctx.params.data.comment + "</p>" // html body
          };
          // send mail with defined transport object
          let info = await transporter.sendMail(mailOptions)
        
          console.log("Message sent: %s", info.messageId);
          // Preview only available when sending through an Ethereal account
          console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        
          // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
          // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
          return info.response;
        }
        main().catch(console.error)
      }
    }
  }

};
