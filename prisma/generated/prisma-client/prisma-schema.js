module.exports = {
        typeDefs: /* GraphQL */ `type AggregateCategory {
  count: Int!
}

type AggregatePlayer {
  count: Int!
}

type AggregateSport {
  count: Int!
}

type AggregateStaff {
  count: Int!
}

type AggregateTeam {
  count: Int!
}

type AggregateUser {
  count: Int!
}

type BatchPayload {
  count: Long!
}

type Category {
  id: ID!
  name: String!
  team(where: TeamWhereInput, orderBy: TeamOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Team!]
}

type CategoryConnection {
  pageInfo: PageInfo!
  edges: [CategoryEdge]!
  aggregate: AggregateCategory!
}

input CategoryCreateInput {
  name: String!
  team: TeamCreateManyInput
}

input CategoryCreateManyInput {
  create: [CategoryCreateInput!]
  connect: [CategoryWhereUniqueInput!]
}

type CategoryEdge {
  node: Category!
  cursor: String!
}

enum CategoryOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type CategoryPreviousValues {
  id: ID!
  name: String!
}

input CategoryScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  AND: [CategoryScalarWhereInput!]
  OR: [CategoryScalarWhereInput!]
  NOT: [CategoryScalarWhereInput!]
}

type CategorySubscriptionPayload {
  mutation: MutationType!
  node: Category
  updatedFields: [String!]
  previousValues: CategoryPreviousValues
}

input CategorySubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: CategoryWhereInput
  AND: [CategorySubscriptionWhereInput!]
  OR: [CategorySubscriptionWhereInput!]
  NOT: [CategorySubscriptionWhereInput!]
}

input CategoryUpdateDataInput {
  name: String
  team: TeamUpdateManyInput
}

input CategoryUpdateInput {
  name: String
  team: TeamUpdateManyInput
}

input CategoryUpdateManyDataInput {
  name: String
}

input CategoryUpdateManyInput {
  create: [CategoryCreateInput!]
  update: [CategoryUpdateWithWhereUniqueNestedInput!]
  upsert: [CategoryUpsertWithWhereUniqueNestedInput!]
  delete: [CategoryWhereUniqueInput!]
  connect: [CategoryWhereUniqueInput!]
  disconnect: [CategoryWhereUniqueInput!]
  deleteMany: [CategoryScalarWhereInput!]
  updateMany: [CategoryUpdateManyWithWhereNestedInput!]
}

input CategoryUpdateManyMutationInput {
  name: String
}

input CategoryUpdateManyWithWhereNestedInput {
  where: CategoryScalarWhereInput!
  data: CategoryUpdateManyDataInput!
}

input CategoryUpdateWithWhereUniqueNestedInput {
  where: CategoryWhereUniqueInput!
  data: CategoryUpdateDataInput!
}

input CategoryUpsertWithWhereUniqueNestedInput {
  where: CategoryWhereUniqueInput!
  update: CategoryUpdateDataInput!
  create: CategoryCreateInput!
}

input CategoryWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  team_every: TeamWhereInput
  team_some: TeamWhereInput
  team_none: TeamWhereInput
  AND: [CategoryWhereInput!]
  OR: [CategoryWhereInput!]
  NOT: [CategoryWhereInput!]
}

input CategoryWhereUniqueInput {
  id: ID
}

scalar Long

type Mutation {
  createCategory(data: CategoryCreateInput!): Category!
  updateCategory(data: CategoryUpdateInput!, where: CategoryWhereUniqueInput!): Category
  updateManyCategories(data: CategoryUpdateManyMutationInput!, where: CategoryWhereInput): BatchPayload!
  upsertCategory(where: CategoryWhereUniqueInput!, create: CategoryCreateInput!, update: CategoryUpdateInput!): Category!
  deleteCategory(where: CategoryWhereUniqueInput!): Category
  deleteManyCategories(where: CategoryWhereInput): BatchPayload!
  createPlayer(data: PlayerCreateInput!): Player!
  updatePlayer(data: PlayerUpdateInput!, where: PlayerWhereUniqueInput!): Player
  updateManyPlayers(data: PlayerUpdateManyMutationInput!, where: PlayerWhereInput): BatchPayload!
  upsertPlayer(where: PlayerWhereUniqueInput!, create: PlayerCreateInput!, update: PlayerUpdateInput!): Player!
  deletePlayer(where: PlayerWhereUniqueInput!): Player
  deleteManyPlayers(where: PlayerWhereInput): BatchPayload!
  createSport(data: SportCreateInput!): Sport!
  updateSport(data: SportUpdateInput!, where: SportWhereUniqueInput!): Sport
  updateManySports(data: SportUpdateManyMutationInput!, where: SportWhereInput): BatchPayload!
  upsertSport(where: SportWhereUniqueInput!, create: SportCreateInput!, update: SportUpdateInput!): Sport!
  deleteSport(where: SportWhereUniqueInput!): Sport
  deleteManySports(where: SportWhereInput): BatchPayload!
  createStaff(data: StaffCreateInput!): Staff!
  updateStaff(data: StaffUpdateInput!, where: StaffWhereUniqueInput!): Staff
  updateManyStaffs(data: StaffUpdateManyMutationInput!, where: StaffWhereInput): BatchPayload!
  upsertStaff(where: StaffWhereUniqueInput!, create: StaffCreateInput!, update: StaffUpdateInput!): Staff!
  deleteStaff(where: StaffWhereUniqueInput!): Staff
  deleteManyStaffs(where: StaffWhereInput): BatchPayload!
  createTeam(data: TeamCreateInput!): Team!
  updateTeam(data: TeamUpdateInput!, where: TeamWhereUniqueInput!): Team
  updateManyTeams(data: TeamUpdateManyMutationInput!, where: TeamWhereInput): BatchPayload!
  upsertTeam(where: TeamWhereUniqueInput!, create: TeamCreateInput!, update: TeamUpdateInput!): Team!
  deleteTeam(where: TeamWhereUniqueInput!): Team
  deleteManyTeams(where: TeamWhereInput): BatchPayload!
  createUser(data: UserCreateInput!): User!
  updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
  updateManyUsers(data: UserUpdateManyMutationInput!, where: UserWhereInput): BatchPayload!
  upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
  deleteUser(where: UserWhereUniqueInput!): User
  deleteManyUsers(where: UserWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

interface Node {
  id: ID!
}

type PageInfo {
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: String
  endCursor: String
}

type Player {
  id: ID!
  name: String!
  salary: Int!
  position: String!
  slug: String!
  photo: String!
}

type PlayerConnection {
  pageInfo: PageInfo!
  edges: [PlayerEdge]!
  aggregate: AggregatePlayer!
}

input PlayerCreateInput {
  name: String!
  salary: Int!
  position: String!
  slug: String!
  photo: String!
}

input PlayerCreateManyInput {
  create: [PlayerCreateInput!]
  connect: [PlayerWhereUniqueInput!]
}

type PlayerEdge {
  node: Player!
  cursor: String!
}

enum PlayerOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  salary_ASC
  salary_DESC
  position_ASC
  position_DESC
  slug_ASC
  slug_DESC
  photo_ASC
  photo_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type PlayerPreviousValues {
  id: ID!
  name: String!
  salary: Int!
  position: String!
  slug: String!
  photo: String!
}

input PlayerScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  salary: Int
  salary_not: Int
  salary_in: [Int!]
  salary_not_in: [Int!]
  salary_lt: Int
  salary_lte: Int
  salary_gt: Int
  salary_gte: Int
  position: String
  position_not: String
  position_in: [String!]
  position_not_in: [String!]
  position_lt: String
  position_lte: String
  position_gt: String
  position_gte: String
  position_contains: String
  position_not_contains: String
  position_starts_with: String
  position_not_starts_with: String
  position_ends_with: String
  position_not_ends_with: String
  slug: String
  slug_not: String
  slug_in: [String!]
  slug_not_in: [String!]
  slug_lt: String
  slug_lte: String
  slug_gt: String
  slug_gte: String
  slug_contains: String
  slug_not_contains: String
  slug_starts_with: String
  slug_not_starts_with: String
  slug_ends_with: String
  slug_not_ends_with: String
  photo: String
  photo_not: String
  photo_in: [String!]
  photo_not_in: [String!]
  photo_lt: String
  photo_lte: String
  photo_gt: String
  photo_gte: String
  photo_contains: String
  photo_not_contains: String
  photo_starts_with: String
  photo_not_starts_with: String
  photo_ends_with: String
  photo_not_ends_with: String
  AND: [PlayerScalarWhereInput!]
  OR: [PlayerScalarWhereInput!]
  NOT: [PlayerScalarWhereInput!]
}

type PlayerSubscriptionPayload {
  mutation: MutationType!
  node: Player
  updatedFields: [String!]
  previousValues: PlayerPreviousValues
}

input PlayerSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: PlayerWhereInput
  AND: [PlayerSubscriptionWhereInput!]
  OR: [PlayerSubscriptionWhereInput!]
  NOT: [PlayerSubscriptionWhereInput!]
}

input PlayerUpdateDataInput {
  name: String
  salary: Int
  position: String
  slug: String
  photo: String
}

input PlayerUpdateInput {
  name: String
  salary: Int
  position: String
  slug: String
  photo: String
}

input PlayerUpdateManyDataInput {
  name: String
  salary: Int
  position: String
  slug: String
  photo: String
}

input PlayerUpdateManyInput {
  create: [PlayerCreateInput!]
  update: [PlayerUpdateWithWhereUniqueNestedInput!]
  upsert: [PlayerUpsertWithWhereUniqueNestedInput!]
  delete: [PlayerWhereUniqueInput!]
  connect: [PlayerWhereUniqueInput!]
  disconnect: [PlayerWhereUniqueInput!]
  deleteMany: [PlayerScalarWhereInput!]
  updateMany: [PlayerUpdateManyWithWhereNestedInput!]
}

input PlayerUpdateManyMutationInput {
  name: String
  salary: Int
  position: String
  slug: String
  photo: String
}

input PlayerUpdateManyWithWhereNestedInput {
  where: PlayerScalarWhereInput!
  data: PlayerUpdateManyDataInput!
}

input PlayerUpdateWithWhereUniqueNestedInput {
  where: PlayerWhereUniqueInput!
  data: PlayerUpdateDataInput!
}

input PlayerUpsertWithWhereUniqueNestedInput {
  where: PlayerWhereUniqueInput!
  update: PlayerUpdateDataInput!
  create: PlayerCreateInput!
}

input PlayerWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  salary: Int
  salary_not: Int
  salary_in: [Int!]
  salary_not_in: [Int!]
  salary_lt: Int
  salary_lte: Int
  salary_gt: Int
  salary_gte: Int
  position: String
  position_not: String
  position_in: [String!]
  position_not_in: [String!]
  position_lt: String
  position_lte: String
  position_gt: String
  position_gte: String
  position_contains: String
  position_not_contains: String
  position_starts_with: String
  position_not_starts_with: String
  position_ends_with: String
  position_not_ends_with: String
  slug: String
  slug_not: String
  slug_in: [String!]
  slug_not_in: [String!]
  slug_lt: String
  slug_lte: String
  slug_gt: String
  slug_gte: String
  slug_contains: String
  slug_not_contains: String
  slug_starts_with: String
  slug_not_starts_with: String
  slug_ends_with: String
  slug_not_ends_with: String
  photo: String
  photo_not: String
  photo_in: [String!]
  photo_not_in: [String!]
  photo_lt: String
  photo_lte: String
  photo_gt: String
  photo_gte: String
  photo_contains: String
  photo_not_contains: String
  photo_starts_with: String
  photo_not_starts_with: String
  photo_ends_with: String
  photo_not_ends_with: String
  AND: [PlayerWhereInput!]
  OR: [PlayerWhereInput!]
  NOT: [PlayerWhereInput!]
}

input PlayerWhereUniqueInput {
  id: ID
}

type Query {
  category(where: CategoryWhereUniqueInput!): Category
  categories(where: CategoryWhereInput, orderBy: CategoryOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Category]!
  categoriesConnection(where: CategoryWhereInput, orderBy: CategoryOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): CategoryConnection!
  player(where: PlayerWhereUniqueInput!): Player
  players(where: PlayerWhereInput, orderBy: PlayerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Player]!
  playersConnection(where: PlayerWhereInput, orderBy: PlayerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PlayerConnection!
  sport(where: SportWhereUniqueInput!): Sport
  sports(where: SportWhereInput, orderBy: SportOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Sport]!
  sportsConnection(where: SportWhereInput, orderBy: SportOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): SportConnection!
  staff(where: StaffWhereUniqueInput!): Staff
  staffs(where: StaffWhereInput, orderBy: StaffOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Staff]!
  staffsConnection(where: StaffWhereInput, orderBy: StaffOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): StaffConnection!
  team(where: TeamWhereUniqueInput!): Team
  teams(where: TeamWhereInput, orderBy: TeamOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Team]!
  teamsConnection(where: TeamWhereInput, orderBy: TeamOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): TeamConnection!
  user(where: UserWhereUniqueInput!): User
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
  usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!
  node(id: ID!): Node
}

type Sport {
  id: ID!
  name: String!
  categories(where: CategoryWhereInput, orderBy: CategoryOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Category!]
}

type SportConnection {
  pageInfo: PageInfo!
  edges: [SportEdge]!
  aggregate: AggregateSport!
}

input SportCreateInput {
  name: String!
  categories: CategoryCreateManyInput
}

type SportEdge {
  node: Sport!
  cursor: String!
}

enum SportOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type SportPreviousValues {
  id: ID!
  name: String!
}

type SportSubscriptionPayload {
  mutation: MutationType!
  node: Sport
  updatedFields: [String!]
  previousValues: SportPreviousValues
}

input SportSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: SportWhereInput
  AND: [SportSubscriptionWhereInput!]
  OR: [SportSubscriptionWhereInput!]
  NOT: [SportSubscriptionWhereInput!]
}

input SportUpdateInput {
  name: String
  categories: CategoryUpdateManyInput
}

input SportUpdateManyMutationInput {
  name: String
}

input SportWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  categories_every: CategoryWhereInput
  categories_some: CategoryWhereInput
  categories_none: CategoryWhereInput
  AND: [SportWhereInput!]
  OR: [SportWhereInput!]
  NOT: [SportWhereInput!]
}

input SportWhereUniqueInput {
  id: ID
}

type Staff {
  id: ID!
  name: String!
  job: String!
  salary: Int!
}

type StaffConnection {
  pageInfo: PageInfo!
  edges: [StaffEdge]!
  aggregate: AggregateStaff!
}

input StaffCreateInput {
  name: String!
  job: String!
  salary: Int!
}

input StaffCreateManyInput {
  create: [StaffCreateInput!]
  connect: [StaffWhereUniqueInput!]
}

type StaffEdge {
  node: Staff!
  cursor: String!
}

enum StaffOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  job_ASC
  job_DESC
  salary_ASC
  salary_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type StaffPreviousValues {
  id: ID!
  name: String!
  job: String!
  salary: Int!
}

input StaffScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  job: String
  job_not: String
  job_in: [String!]
  job_not_in: [String!]
  job_lt: String
  job_lte: String
  job_gt: String
  job_gte: String
  job_contains: String
  job_not_contains: String
  job_starts_with: String
  job_not_starts_with: String
  job_ends_with: String
  job_not_ends_with: String
  salary: Int
  salary_not: Int
  salary_in: [Int!]
  salary_not_in: [Int!]
  salary_lt: Int
  salary_lte: Int
  salary_gt: Int
  salary_gte: Int
  AND: [StaffScalarWhereInput!]
  OR: [StaffScalarWhereInput!]
  NOT: [StaffScalarWhereInput!]
}

type StaffSubscriptionPayload {
  mutation: MutationType!
  node: Staff
  updatedFields: [String!]
  previousValues: StaffPreviousValues
}

input StaffSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: StaffWhereInput
  AND: [StaffSubscriptionWhereInput!]
  OR: [StaffSubscriptionWhereInput!]
  NOT: [StaffSubscriptionWhereInput!]
}

input StaffUpdateDataInput {
  name: String
  job: String
  salary: Int
}

input StaffUpdateInput {
  name: String
  job: String
  salary: Int
}

input StaffUpdateManyDataInput {
  name: String
  job: String
  salary: Int
}

input StaffUpdateManyInput {
  create: [StaffCreateInput!]
  update: [StaffUpdateWithWhereUniqueNestedInput!]
  upsert: [StaffUpsertWithWhereUniqueNestedInput!]
  delete: [StaffWhereUniqueInput!]
  connect: [StaffWhereUniqueInput!]
  disconnect: [StaffWhereUniqueInput!]
  deleteMany: [StaffScalarWhereInput!]
  updateMany: [StaffUpdateManyWithWhereNestedInput!]
}

input StaffUpdateManyMutationInput {
  name: String
  job: String
  salary: Int
}

input StaffUpdateManyWithWhereNestedInput {
  where: StaffScalarWhereInput!
  data: StaffUpdateManyDataInput!
}

input StaffUpdateWithWhereUniqueNestedInput {
  where: StaffWhereUniqueInput!
  data: StaffUpdateDataInput!
}

input StaffUpsertWithWhereUniqueNestedInput {
  where: StaffWhereUniqueInput!
  update: StaffUpdateDataInput!
  create: StaffCreateInput!
}

input StaffWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  job: String
  job_not: String
  job_in: [String!]
  job_not_in: [String!]
  job_lt: String
  job_lte: String
  job_gt: String
  job_gte: String
  job_contains: String
  job_not_contains: String
  job_starts_with: String
  job_not_starts_with: String
  job_ends_with: String
  job_not_ends_with: String
  salary: Int
  salary_not: Int
  salary_in: [Int!]
  salary_not_in: [Int!]
  salary_lt: Int
  salary_lte: Int
  salary_gt: Int
  salary_gte: Int
  AND: [StaffWhereInput!]
  OR: [StaffWhereInput!]
  NOT: [StaffWhereInput!]
}

input StaffWhereUniqueInput {
  id: ID
}

type Subscription {
  category(where: CategorySubscriptionWhereInput): CategorySubscriptionPayload
  player(where: PlayerSubscriptionWhereInput): PlayerSubscriptionPayload
  sport(where: SportSubscriptionWhereInput): SportSubscriptionPayload
  staff(where: StaffSubscriptionWhereInput): StaffSubscriptionPayload
  team(where: TeamSubscriptionWhereInput): TeamSubscriptionPayload
  user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
}

type Team {
  id: ID!
  name: String!
  budget: Int!
  wins: Int!
  draws: Int!
  defeats: Int!
  points: Int!
  slug: String!
  players(where: PlayerWhereInput, orderBy: PlayerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Player!]
  staff(where: StaffWhereInput, orderBy: StaffOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Staff!]
}

type TeamConnection {
  pageInfo: PageInfo!
  edges: [TeamEdge]!
  aggregate: AggregateTeam!
}

input TeamCreateInput {
  name: String!
  budget: Int!
  wins: Int!
  draws: Int!
  defeats: Int!
  points: Int!
  slug: String!
  players: PlayerCreateManyInput
  staff: StaffCreateManyInput
}

input TeamCreateManyInput {
  create: [TeamCreateInput!]
  connect: [TeamWhereUniqueInput!]
}

type TeamEdge {
  node: Team!
  cursor: String!
}

enum TeamOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  budget_ASC
  budget_DESC
  wins_ASC
  wins_DESC
  draws_ASC
  draws_DESC
  defeats_ASC
  defeats_DESC
  points_ASC
  points_DESC
  slug_ASC
  slug_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type TeamPreviousValues {
  id: ID!
  name: String!
  budget: Int!
  wins: Int!
  draws: Int!
  defeats: Int!
  points: Int!
  slug: String!
}

input TeamScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  budget: Int
  budget_not: Int
  budget_in: [Int!]
  budget_not_in: [Int!]
  budget_lt: Int
  budget_lte: Int
  budget_gt: Int
  budget_gte: Int
  wins: Int
  wins_not: Int
  wins_in: [Int!]
  wins_not_in: [Int!]
  wins_lt: Int
  wins_lte: Int
  wins_gt: Int
  wins_gte: Int
  draws: Int
  draws_not: Int
  draws_in: [Int!]
  draws_not_in: [Int!]
  draws_lt: Int
  draws_lte: Int
  draws_gt: Int
  draws_gte: Int
  defeats: Int
  defeats_not: Int
  defeats_in: [Int!]
  defeats_not_in: [Int!]
  defeats_lt: Int
  defeats_lte: Int
  defeats_gt: Int
  defeats_gte: Int
  points: Int
  points_not: Int
  points_in: [Int!]
  points_not_in: [Int!]
  points_lt: Int
  points_lte: Int
  points_gt: Int
  points_gte: Int
  slug: String
  slug_not: String
  slug_in: [String!]
  slug_not_in: [String!]
  slug_lt: String
  slug_lte: String
  slug_gt: String
  slug_gte: String
  slug_contains: String
  slug_not_contains: String
  slug_starts_with: String
  slug_not_starts_with: String
  slug_ends_with: String
  slug_not_ends_with: String
  AND: [TeamScalarWhereInput!]
  OR: [TeamScalarWhereInput!]
  NOT: [TeamScalarWhereInput!]
}

type TeamSubscriptionPayload {
  mutation: MutationType!
  node: Team
  updatedFields: [String!]
  previousValues: TeamPreviousValues
}

input TeamSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: TeamWhereInput
  AND: [TeamSubscriptionWhereInput!]
  OR: [TeamSubscriptionWhereInput!]
  NOT: [TeamSubscriptionWhereInput!]
}

input TeamUpdateDataInput {
  name: String
  budget: Int
  wins: Int
  draws: Int
  defeats: Int
  points: Int
  slug: String
  players: PlayerUpdateManyInput
  staff: StaffUpdateManyInput
}

input TeamUpdateInput {
  name: String
  budget: Int
  wins: Int
  draws: Int
  defeats: Int
  points: Int
  slug: String
  players: PlayerUpdateManyInput
  staff: StaffUpdateManyInput
}

input TeamUpdateManyDataInput {
  name: String
  budget: Int
  wins: Int
  draws: Int
  defeats: Int
  points: Int
  slug: String
}

input TeamUpdateManyInput {
  create: [TeamCreateInput!]
  update: [TeamUpdateWithWhereUniqueNestedInput!]
  upsert: [TeamUpsertWithWhereUniqueNestedInput!]
  delete: [TeamWhereUniqueInput!]
  connect: [TeamWhereUniqueInput!]
  disconnect: [TeamWhereUniqueInput!]
  deleteMany: [TeamScalarWhereInput!]
  updateMany: [TeamUpdateManyWithWhereNestedInput!]
}

input TeamUpdateManyMutationInput {
  name: String
  budget: Int
  wins: Int
  draws: Int
  defeats: Int
  points: Int
  slug: String
}

input TeamUpdateManyWithWhereNestedInput {
  where: TeamScalarWhereInput!
  data: TeamUpdateManyDataInput!
}

input TeamUpdateWithWhereUniqueNestedInput {
  where: TeamWhereUniqueInput!
  data: TeamUpdateDataInput!
}

input TeamUpsertWithWhereUniqueNestedInput {
  where: TeamWhereUniqueInput!
  update: TeamUpdateDataInput!
  create: TeamCreateInput!
}

input TeamWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  budget: Int
  budget_not: Int
  budget_in: [Int!]
  budget_not_in: [Int!]
  budget_lt: Int
  budget_lte: Int
  budget_gt: Int
  budget_gte: Int
  wins: Int
  wins_not: Int
  wins_in: [Int!]
  wins_not_in: [Int!]
  wins_lt: Int
  wins_lte: Int
  wins_gt: Int
  wins_gte: Int
  draws: Int
  draws_not: Int
  draws_in: [Int!]
  draws_not_in: [Int!]
  draws_lt: Int
  draws_lte: Int
  draws_gt: Int
  draws_gte: Int
  defeats: Int
  defeats_not: Int
  defeats_in: [Int!]
  defeats_not_in: [Int!]
  defeats_lt: Int
  defeats_lte: Int
  defeats_gt: Int
  defeats_gte: Int
  points: Int
  points_not: Int
  points_in: [Int!]
  points_not_in: [Int!]
  points_lt: Int
  points_lte: Int
  points_gt: Int
  points_gte: Int
  slug: String
  slug_not: String
  slug_in: [String!]
  slug_not_in: [String!]
  slug_lt: String
  slug_lte: String
  slug_gt: String
  slug_gte: String
  slug_contains: String
  slug_not_contains: String
  slug_starts_with: String
  slug_not_starts_with: String
  slug_ends_with: String
  slug_not_ends_with: String
  players_every: PlayerWhereInput
  players_some: PlayerWhereInput
  players_none: PlayerWhereInput
  staff_every: StaffWhereInput
  staff_some: StaffWhereInput
  staff_none: StaffWhereInput
  AND: [TeamWhereInput!]
  OR: [TeamWhereInput!]
  NOT: [TeamWhereInput!]
}

input TeamWhereUniqueInput {
  id: ID
}

type User {
  id: ID!
  email: String
  username: String
  password: String
  slug: String
  team(where: TeamWhereInput, orderBy: TeamOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Team!]
  image: String
}

type UserConnection {
  pageInfo: PageInfo!
  edges: [UserEdge]!
  aggregate: AggregateUser!
}

input UserCreateInput {
  email: String
  username: String
  password: String
  slug: String
  team: TeamCreateManyInput
  image: String
}

type UserEdge {
  node: User!
  cursor: String!
}

enum UserOrderByInput {
  id_ASC
  id_DESC
  email_ASC
  email_DESC
  username_ASC
  username_DESC
  password_ASC
  password_DESC
  slug_ASC
  slug_DESC
  image_ASC
  image_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type UserPreviousValues {
  id: ID!
  email: String
  username: String
  password: String
  slug: String
  image: String
}

type UserSubscriptionPayload {
  mutation: MutationType!
  node: User
  updatedFields: [String!]
  previousValues: UserPreviousValues
}

input UserSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: UserWhereInput
  AND: [UserSubscriptionWhereInput!]
  OR: [UserSubscriptionWhereInput!]
  NOT: [UserSubscriptionWhereInput!]
}

input UserUpdateInput {
  email: String
  username: String
  password: String
  slug: String
  team: TeamUpdateManyInput
  image: String
}

input UserUpdateManyMutationInput {
  email: String
  username: String
  password: String
  slug: String
  image: String
}

input UserWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  email: String
  email_not: String
  email_in: [String!]
  email_not_in: [String!]
  email_lt: String
  email_lte: String
  email_gt: String
  email_gte: String
  email_contains: String
  email_not_contains: String
  email_starts_with: String
  email_not_starts_with: String
  email_ends_with: String
  email_not_ends_with: String
  username: String
  username_not: String
  username_in: [String!]
  username_not_in: [String!]
  username_lt: String
  username_lte: String
  username_gt: String
  username_gte: String
  username_contains: String
  username_not_contains: String
  username_starts_with: String
  username_not_starts_with: String
  username_ends_with: String
  username_not_ends_with: String
  password: String
  password_not: String
  password_in: [String!]
  password_not_in: [String!]
  password_lt: String
  password_lte: String
  password_gt: String
  password_gte: String
  password_contains: String
  password_not_contains: String
  password_starts_with: String
  password_not_starts_with: String
  password_ends_with: String
  password_not_ends_with: String
  slug: String
  slug_not: String
  slug_in: [String!]
  slug_not_in: [String!]
  slug_lt: String
  slug_lte: String
  slug_gt: String
  slug_gte: String
  slug_contains: String
  slug_not_contains: String
  slug_starts_with: String
  slug_not_starts_with: String
  slug_ends_with: String
  slug_not_ends_with: String
  team_every: TeamWhereInput
  team_some: TeamWhereInput
  team_none: TeamWhereInput
  image: String
  image_not: String
  image_in: [String!]
  image_not_in: [String!]
  image_lt: String
  image_lte: String
  image_gt: String
  image_gte: String
  image_contains: String
  image_not_contains: String
  image_starts_with: String
  image_not_starts_with: String
  image_ends_with: String
  image_not_ends_with: String
  AND: [UserWhereInput!]
  OR: [UserWhereInput!]
  NOT: [UserWhereInput!]
}

input UserWhereUniqueInput {
  id: ID
  email: String
  username: String
}
`
      }
    