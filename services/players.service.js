"use strict";

const { prisma } = require('../prisma/generated/prisma-client');

module.exports = {
	name: "players",

	settings: {
		},

	actions: {
		listPlayers: {
			params:{
				data: {
					type: "object", props:{
						id_team: {type: "string"}
					}
				}
			},
			async handler(ctx){
					const size = `query teams($data: TeamWhereInput!){
  													teams(where: $data){
    														players{
      															id
    														}
  													}
											}`

					const variable = {"data":{"id": ctx.params.data.id_team}};

					const total = await prisma.$graphql(size, variable);

					console.log(total.teams);


					const query = `query firstPlayer($data: TeamWhereUniqueInput!){
  														team(where:$data){
    														players(first:1){
     																	name
      																	id
      																	salary
      																	position
      																	photo
    														}
  														}
													}`
					
					const res = await prisma.$graphql(query, variable);

					const result = {size: total.teams, player: res}
					return result;
			}
		},
		setPage:{
			params:{
				data:{
					type: "object", props:{
						id_team: {type: "string"}
					}
				},skip:{type: "number"}
			},
			async handler(ctx){
				const query = `query firstPlayer($data: TeamWhereUniqueInput!, $skip: Int){
  																		team(where:$data){
    																		players(first:1, skip:$skip){
      																				name
      																				id
      																				salary
      																				position
      																				photo
    																		}
																		  }
																		}`
				const variable = {"data":{"id": ctx.params.data.id_team}, "skip": ctx.params.skip};

				const res = await prisma.$graphql(query, variable);
				console.log(res);
				return res;
			}
		}
	}
}