"use strict"
const { ServiceBroker } = require("moleculer");
const { prisma } = require('../prisma/generated/prisma-client');
const broker = new ServiceBroker();

module.exports = {
	name: "faker",

	settings: {
		},

	actions: {
		generateDummies:{
			/**
			* This is the main function
			* I need use async because JS is asyncronous
			* and i need use promises
			*/
			async handler(){
				/**
				*First of all, we declared the numbers of dummies we want create
				*/
				const staffNumber = 40;
				const playerNumber = 40;
				const teamNumber = 20;
				const categoryNumber = 10;
				const sportNumber = 5;
				const userNumber = 20;
				/**
				*Make a loop to create the number of dummies selected
				*/
				for(let i = 0; i < staffNumber; i++){
					this.createStaff();
				}
				/**
				*The same with players
				*/
				for(let i = 0; i < playerNumber; i++){
					this.createPlayer();
				}
				/**
				*Now, we need the list of players created before, to make related tables
				*/
				const query = `
					query staff{
					  staffs{
					    id
					  }
					}
				`
				/**
				*Declare the query
				* and use it and insert into a const
				*/
				const staff = await prisma.$graphql(query);
				/**
				*Have now the staffsList, now we need a playersList, we do the same
				*/
				const query2 = `
					query player{
					  players{
					    id
					  }
					}
				`
				const players = await prisma.$graphql(query2);
				/**
				*Okey, now we need an algorithm
				* First of all, check number of players and staffs
				* later we divide it between the number of equipment before declared
				* Finally we use the toFixed method to round the result
				* but toFixed return a string and I need an Integer, then use parseInt to convert it
				*/
				const totalPlayers = parseInt(((Object(players.players).length)/teamNumber).toFixed(0));
				const totalStaffs = parseInt(((Object(staff.staffs).length)/teamNumber).toFixed(0));
				/**
				* Made all the preparations, we created a loop
				* and i pass the total player/staff per team
				* player list and staff list
				* and finally the index of the loop
				*/
				for(let i = 0; i < teamNumber; i++){
					this.createTeam(totalPlayers, totalStaffs, i+1, players.players, staff.staffs);
				}
				/**
				* This is the same that before
				* but we only made 1 time, because only have one reference
				*/
				const query3 = `
					query team{
					  teams{
					  	id
					  }
					}
				`

				const teams = await prisma.$graphql(query3);
				const totalTeams = parseInt(((Object(teams.teams).length)/categoryNumber).toFixed(0));

				for(let i = 0; i < categoryNumber; i ++){
					this.createCategory(totalTeams, teams.teams, i+1);
				}
				/**
				* Again the same, to made related tables categories/sport
				*/
				const query4 = `
					query category{
						categories{
							id
						}
					}
				`

				const categories = await prisma.$graphql(query4);
				const totalCategories = parseInt(((Object(categories.categories).length)/sportNumber).toFixed(0));
				for(let i = 0; i < sportNumber; i ++){
					this.createSport(totalCategories, categories.categories, i+1);
				}
			}
		}
	},
	methods : {
		/**
		* I divide all the functions into a small methods
		* to make the source readable
		*/
		async createStaff(){
			/**
			* First of all we create the broker service
			* and set the languaje configuration
			*/
			broker.createService(require("moleculer-fake"), { settings: { locale: "es-ES" }});
			/**
			* this pieces of code generate a random, name, string, number...
			*/
			const name = await broker.call("fake.name");
			const job = await broker.call("fake.string");
			const salary = await broker.call("fake.number", {min: 1000, max: 10000});
			/**
			* Insert all the information about the staff into a const
			* and make a mutation
			*/
			const data = {"data" : { "name" : name, "job" : job, "salary" : salary} };

			const mutation = `
							mutation createStaff($data: StaffCreateInput!){
  								createStaff(data: $data){
    								id
								  }
								}
							`
			/**
			* Insert into prisma
			*/
			await prisma.$graphql(mutation, data);
		},
		async createPlayer(){
			/**
			* We create a library of positions
			*/
			const pos = ["Goalkeeper", "Defender", "Midfielder", "Striker"];
			broker.createService(require("moleculer-fake"), { settings: { locale: "es-ES" }});
			const name = await broker.call("fake.name");
			/**
			* The slug library does not work within a promise, so I decided to create my own function
			*/
			const slug = name.split(".").join(" ").split(" ").join("-").toLowerCase();
			/**
			* This number is to extract the position of the library variable that we have declared before
			*/
			const number = await broker.call("fake.number", {min:0,max:3});
			const position = pos[number];
			const photo = await broker.call("fake.image");
			const salary = await broker.call("fake.number", {min: 10000, max: 1000000});
			const data = {"data" : { "name" : name, "position" : position, "photo" : photo, "salary" : salary, "slug" : slug }};
			const mutation = `
				mutation createPlayer($data: PlayerCreateInput!){
				  createPlayer(data: $data){
				    id
				  }
				}
			`
			await prisma.$graphql(mutation, data);
		},
		async createTeam(nPlayers, nStaffs, i, players, staffs){
			/**
			* This function is more difficult because need related tables
			* is very important, firsr create the players and later the teams, because you
			* need the players to insert into the teams
			*/
			/**
			* This is to calculate the beginning of the index of the loop and the end
			*/
			const minPlayers = (i-1) * nPlayers;
			const maxPlayers = i * nPlayers;
			const minStaffs = (i-1) * nStaffs;
			const maxStaffs = i * nStaffs;
			/**
			* Initialize the arrays list, to insert the information
			*/
			let playersList = [];
			let staffsList = [];
			const lib = [" F.C.", " C.F", " S.A.D"];
			broker.createService(require("moleculer-fake"), { settings: { locale: "es-ES" }});
			const name = await broker.call("fake.firstName");
			const randomPrefix = await broker.call("fake.number", {min: 0,max: 2});
			/**
			* To genearte a random team name, we add a random name more a randome prefix
			*/
			const completeName = name + lib[randomPrefix];
			const slug = completeName.split(".").join(" ").split(" ").join("-").toLowerCase();
			/**
			* I want all the teams have the same numbers of match
			* So I generate a number on the total, I know the rest,
			* that's the maximum number of draws and the rest are the defeats
			*/
			const wins = await broker.call("fake.number", {min: 0, max: 37});
			const total = 38 - wins;
			const draws = await broker.call("fake.number",{min: 0, max: total});
			const defeats = total - draws;
			const points = (wins * 3) + draws;
			const budget = await broker.call("fake.number", {min: 1000000, max: 10000000});
			for(let i = minPlayers; i<maxPlayers; i++){
				playersList.push(players[i]);
			}
			for(let i = minStaffs; i < maxStaffs; i++){
				staffsList.push(staffs[i]);
			}

			const mutation = `
				mutation createTeam($data: TeamCreateInput!){
				  createTeam(data: $data){
				    id
				  }
				}
			`
			const variable = {"data": {"name": completeName, "budget": budget, "wins": wins, "draws": draws, "defeats": defeats,"points": points, "players":{connect: playersList}, "staff": {connect: staffsList}, "slug": slug}};
			await prisma.$graphql(mutation, variable);
		},
		async createCategory(nTeams, teams, i){
			/**
			* This method is very similar to the others and does not bring anything new
			*/
			const minTeams = (i-1) * nTeams;
			const maxTeams = i * nTeams;
			let teamsList = [];
			broker.createService(require("moleculer-fake"), { settings: { locale: "es-ES" }});
			const name = await broker.call("fake.string");
			for(let i = minTeams; i < maxTeams; i ++){
				if(teams[i]){
					teamsList.push(teams[i]);
				}
			};
			const mutation = `
				mutation createCategory($data: CategoryCreateInput!){
				  createCategory(data: $data){
				    id
				  }
				}
			`

			const variable = {"data": {"name": name, "team": {connect: teamsList}}};
			await prisma.$graphql(mutation, variable);
		},
		async createSport(nCategories, categories, i){
			/**
			* This method is very similar to the others and does not bring anything new
			*/
			const minCategories = (i-1) * nCategories;
			const maxCategories = i * nCategories;
			let categoriesList = [];
			broker.createService(require("moleculer-fake"), { settings: { locale: "es-ES" }});
			const name = await broker.call("fake.string");
			for(let i = minCategories; i < maxCategories; i ++){
				if(categories[i]){
					categoriesList.push(categories[i]);
				}
			};
			const mutation = `
				mutation createSport($data: SportCreateInput!){
				  createSport(data: $data){
				    id
				  }
				}
			`

			const variable = {"data": {"name": name, "categories": {connect: categoriesList}}};
			await prisma.$graphql(mutation, variable);
		}
	}
}